// Prijava.spec.js created with Cypress
//
// Start writing your Cypress tests below!
// If you're unfamiliar with how Cypress works,
// check out the link below and learn how to write your first test:
// https://on.cypress.io/writing-first-test

it("PogresnaPrijava", function () {
  /* ==== Generated with Cypress Studio ==== */
  cy.visit("http://localhost:8080/#/prijava");
  cy.get('[data-cy="email"]').clear();
  cy.get('[data-cy="email"]').type("hjgjhg");
  cy.get('[data-cy="sifra"]').clear();
  cy.get('[data-cy="sifra"]').type("gfdgf");
  cy.get(".block").click();
  /* ==== End Cypress Studio ==== */
});