const routes = [
  {
    path: "/",
    component: () => import("layouts/PrazanLayout.vue"),
    children: [
      {
        path: "",
        component: () => import("pages/IndexPage.vue"),
        meta: { requiresAuth: true },
      },
      {
        path: "/galerija",
        component: () => import("src/pages/PregledSlika.vue"),
        meta: { requiresAuth: true },
      },
      {
        path: "/izbor-clouda",
        component: () => import("src/pages/IzborClouda.vue"),
        meta: { requiresAuth: true },
      },
      {
        path: "/prijava",
        component: () => import("pages/PrijavaKorisnika.vue"),
      },
      {
        path: "/registracija",
        component: () => import("pages/RegistracijaKorisnika.vue"),
      },
    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: "/:catchAll(.*)*",
    component: () => import("pages/ErrorNotFound.vue"),
  },
];

export default routes;
